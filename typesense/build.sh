#!/usr/bin/env sh

tags=$(curl -s https://api.github.com/repos/typesense/typesense/tags | jq -r '.[].name')

for tag in $tags; do
    git clone -b $tag https://github.com/typesense/typesense.git tmp/typesense-$tag
    bash tmp/typesense-$tag/docker-build.sh
    rm -rf tmp/typesense-$tag
    clear
done